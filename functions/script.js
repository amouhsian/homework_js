
// **************** FUNCTIONS *************************

// Create a JavaScript function to find the Nth non-repeated value in the given array (numbers only). 
// There is at least 1 non-repeated value in the array.
 
console.log('Create a JavaScript function to find the Nth non-repeated value in the given array (numbers only). ');
console.log('There is at least 1 non-repeated value in the array. ');

function nThNoRepeatedValue(arr, n) {
    const count = {};
    arr.forEach(element => {
        count[element] = (count[element] || 0) + 1;
    });

    for (let i = n - 1; i < arr.length; ++i) {
        if (count[arr[i]] === 1) {
            return arr[i];
        }
    }
};
console.log(`Nth non-repeated value = [${nThNoRepeatedValue([1,2,3,2,5,3],2)}]`);
console.log(`Nth non-repeated value = ["${nThNoRepeatedValue(["a", "b","c","d","a","c"],1)}"]`);


// Create a JavaScript function whicj converts the given array of numbers to an array of booleans. 
// Each element is true if the number is a prime number, and false otherwise.

console.log('Create a JavaScript function which converts the given array of numbers to an array of booleans.');
console.log(' Each element is true if the number is a prime number, and false otherwise.');

function primeValues(arr) {
    let primeArray = [];

    arr.forEach(function (element) {
        const isPrime = checkPrime(element);
        if (isPrime) {
            primeArray.push(true);
        } else {
            primeArray.push(false);
        }
    });
    return primeArray;
   
}

function checkPrime(number) {
    if (number <= 1) {
        return false;
    } else {
        for (let i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

};

console.log(`true and false elements = [${primeValues([1,2,3,4,5])}]`);



