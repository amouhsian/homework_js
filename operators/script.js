

// *************** Operators, Controls, and Loops  *********************************


// Create a JavaScript function to find difference between two arrays

console.log('Create a JavaScript function to find difference between two arrays');
function arrayDiff(array1, array2) {
   
    let arr = [];
    for (let i = 0; i<array1.length; ++i) {
     
        if (!array2.includes(array1[i])) {
              arr.push(array1[i]);  
            }
    }
    
      for (let i = 0; i<array2.length; ++i) {
   
        if (!array1.includes(array2[i])) {
              arr.push(array2[i]);  
            }
    }
    console.log(array1);
    console.log(array2);
    return arr;
};

console.log(`difference of 2 arrays = [${arrayDiff([1,2,3,4,5],[1,2,3])}]`);


// ******************************************************************************

//Create a JavaScript function which gets two numbers and iterates over them. Check if it is even or odd. 

console.log('Create a JavaScript function which gets two numbers and iterates over them. Check if it is even or odd. ');
function evenOrOdd(x, y) {
   
    let arrOddEven = [];
    for(let i = x; i <= y; i++) {
        if (i % 2 == 0){
            arrOddEven.push(`${i} is even`)
        } else {
            arrOddEven.push(`${i} is odd`)
        }
    }
    return arrOddEven;
    };
    console.log(evenOrOdd(1,5));


// ********************************************************************************

// Create a JavaScript function to get a sum of all numbers in the given range.

console.log(' Create a JavaScript function to get a sum of all numbers in the given range.');

    function rangeSum(x, y) {
   
        let sum = 0;
        for(let i = x; i <= y; i++){
            sum +=i;
        }
        return sum;
    };
    const result = rangeSum();
    console.log(`Sum of the range = ${rangeSum(1,3)}`);
