
// ********************** DATATYPES ****************************

//
console.log('Create the function "backToFront" which gets a string and symbols count. And it should return a string. For example:');

function backToFront(str, symbolsCount) {
    

    if (symbolsCount > str.length) {
        return str;
    }
    const matchStr = str.slice(-symbolsCount);
    const backFront = str.concat(matchStr);
    return matchStr.concat(backFront);
   
};

console.log(`New string = "${backToFront("Hello", 2)}"`);


// *********************************************************************************
// Create the function "nearest" to find a value which is nearest to z from two given values (x and Y);


console.log('Create the function "nearest" to find a value which is nearest to z from two given values (x and Y)');

function nearest(z, x, y) {
  
    let arr = [];
    arr.push(z);
    arr.push(y);
    const closest = arr.reduce(function(prev, curr) {
        return (Math.abs(curr - z) < Math.abs(prev - z) ? curr : prev);
    });
    return closest;
};
console.log('nearest = ',nearest(10,20,30));

// *******************************************************************************

// Create the function "removeDuplicate" to remove all duplicated values from array; do not use a set. 

console.log('Create the function "removeDuplicate" to remove all duplicated values from array; do not use a set.');


function removeDuplicate(arr) {

        return arr.filter((item, position) => arr.indexOf(item) === position);
};
console.log(`array without duplicates = [${removeDuplicate([1,2,3,2,1])}]`);
console.log(`array without duplicates = [${removeDuplicate(['a','b','c','a','c'])}]`);

