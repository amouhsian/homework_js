//Is valid JSON

console.log("Create the function '(isValidJSON)' which validates if the string is valid JSON or not. ");


function isValidJSON(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

isValidJSON('{"a: 2');
// isValidJSON('{"a": 2}');



// **************** Create the function "greeting" which returns a greeting string. For example:

console.log('Create the function "greeting" which returns a greeting string.');


function greeting(str){
    let obj= {...str};
     console.log(`hello my name is ${obj.name} ${obj.surname}  and I am  ${obj.age} years old` );
 }
 greeting({name: 'Bill', surname: 'Jacobson', age: 33}); 

 
 
 // Create the function unique which takes an array of numbers with duplicates and returns
 // an array of numbers without duplicates. Use a set. 

 function unique(arr){
    let set  = new Set();
    for(let i = 0; i < arr.length; i++){
        set.add(arr[i]);
    }
  return set;
}
console.log(unique([1,2,3,4,2,3,5]));



//Create the function generator which takes an array of Lego blocks and return them one by one.


console.log("Create the function generator which takes an array of Lego blocks and return them one by one.");


function* generator(arr) {

    for(let i = 0; i <arr.length; i ++){
        yield arr[i];
    }
 }
 let arrGen = generator(['brick', 'plate', 'minifigure', 'slope']);
 
 console.log(arrGen.next().value);
 console.log(arrGen.next().value);
 console.log(arrGen.next().value);
 console.log(arrGen.next().value);

