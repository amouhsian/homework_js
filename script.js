
// Sum of 2 numbers

function add(x){
    return function(y){
        return x+y;
    }
}
console.log('Sum of 2 numbers(x,y) = ',add(1)(2));


// Average Ages of Users

const users = [
    {
        gender: 'male',
        age: 24,
    },
    {
        gender: 'female',
        age: 21,
    },
    {
        gender: 'male',
        age: 36,
    }
];
let total = 0;
users.forEach((element) => {
    total +=element.age;
});
console.log('Average age of users = ',total / users.length);



// function that gets an array and returns unique values with a new array


function repeat(arr) {
    const arr1 = [];
    let j;
    for (let i = 0; i < arr.length; i++) {
        for ( j = 0; j < arr.length; j++)
            if (i != j && arr[i] == arr[j])
                break;
        if (j == arr.length)
            arr1.push(arr[i]);
    }
    return arr1;
}
console.log('Function that gets an array and returns unique values with a new array');
console.log(repeat([10, 5, 6, 10, 6, 7]));

//  Create a createCounter function that returns one more value from each call and starts at 0

console.log(" Create a createCounter function that returns one more value from each call and starts at 0");
function createCounter() {
    let i = 0;
    return function () {
        return i++;
    }
}
let getCounter = createCounter();
console.log(getCounter());
console.log(getCounter());
console.log(getCounter());

