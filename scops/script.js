
//Create function "createCounter" to generate numbers. It should receive the start value, which by default is equal to 0.

console.log("  Create function 'createCounter' to generate numbers. It should receive the start value, which by default is equal to 0");

function createCounter() {
    let i = 0;
    return function () {
        return i++;
    }
}
let getCounter = createCounter();
console.log(getCounter());
console.log(getCounter());
console.log(getCounter());


//Create function "multiply" to multiple values
console.log("Create function 'multiply' to multiple values");

function add(x){
    return function(y){
        return function(z){
            return x*y*z;
        }
    }
}
console.log('x*y*z = ',add(2)(3)(5));

